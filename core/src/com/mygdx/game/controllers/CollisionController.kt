package com.mygdx.game.controllers

import com.mygdx.game.objects.Panda
import com.mygdx.game.objects.Snake

/**
 * Created by stanislavvasylkovskyi on 11/19/16.
 */

class CollisionController {

    fun checkPandaCollision(snake: Snake, panda: Panda): Boolean {
        if (isCollide(snake, panda)) {
            snake.addPart()
            panda.isPandaAvailable = false
            panda.checkAndPlacePanda(snake)
            return true
        }
        return false
    }

    private fun isCollide(snake: Snake, panda: Panda): Boolean {
        return panda.isPandaAvailable &&
                panda.pandaX <= snake.snakeX && panda.pandaX + 48 > snake.snakeX &&
                panda.pandaY <= snake.snakeY && panda.pandaY + 48 > snake.snakeY
    }
}
