package com.mygdx.game.controllers

import com.badlogic.gdx.math.MathUtils
import com.mygdx.game.core.Directions
import com.mygdx.game.objects.Panda
import com.mygdx.game.objects.Snake

import java.util.ArrayList


/**
 * Created by stanislavvasylkovskyi on 11/20/16.
 */

class Bot {

    private val possibleDirectionsTuple = PossibleDirectionsTuple()

    fun getDirection(snake: Snake, panda: Panda): Directions {
        val lower = isPandaLower(snake.snakeY, panda.pandaY)
        val righter = isPandaRight(snake.snakeX, panda.pandaX)

        if (lower) {
            possibleDirectionsTuple.setY(Directions.DOWN)
        } else
            possibleDirectionsTuple.setY(Directions.UP)

        if (snake.snakeY == panda.pandaY) possibleDirectionsTuple.setY(Directions.NONE)

        if (righter) {
            possibleDirectionsTuple.setX(Directions.RIGHT)
        } else
            possibleDirectionsTuple.setX(Directions.LEFT)

        if (snake.snakeX == panda.pandaX) possibleDirectionsTuple.setX(Directions.NONE)

        return possibleDirectionsTuple.direction
    }

    private fun isPandaLower(snakeY: Int, pandaY: Int): Boolean {
        return snakeY > pandaY
    }

    private fun isPandaRight(snakeX: Int, pandaX: Int): Boolean {
        return snakeX < pandaX
    }

    private inner class PossibleDirectionsTuple {
        private var x: Directions = Directions.LEFT
        private var y: Directions = Directions.DOWN

        fun setX(x: Directions) {
            this.x = x
        }

        fun setY(y: Directions) {
            this.y = y
        }

        val direction: Directions
            get() {
                if (x == Directions.NONE) return y
                if (y == Directions.NONE) return x
                return if (MathUtils.random(1) == 1) x else y
            }
    }
}
