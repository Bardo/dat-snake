package com.mygdx.game.controllers

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.mygdx.game.core.Directions

/**
 * Created by stanislavvasylkovskyi on 11/19/16.
 */

class DirectionController {

    internal var directions = Directions.NONE

    val snakeDirection: Directions
        get() {
            if (Gdx.input.isKeyPressed(Input.Keys.LEFT))
                directions = Directions.LEFT
            if (Gdx.input.isKeyPressed(Input.Keys.RIGHT))
                directions = Directions.RIGHT
            if (Gdx.input.isKeyPressed(Input.Keys.UP))
                directions = Directions.UP
            if (Gdx.input.isKeyPressed(Input.Keys.DOWN))
                directions = Directions.DOWN
            return directions
        }


    val deltaChange: Float
        get() {
            if (Gdx.input.isKeyPressed(Input.Keys.A))
                return 0.1f
            if (Gdx.input.isKeyPressed(Input.Keys.Z))
                return -0.1f
            return 0f
        }

}
