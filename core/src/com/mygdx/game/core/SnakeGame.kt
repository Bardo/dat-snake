package com.mygdx.game.core

import com.badlogic.gdx.Game
import com.mygdx.game.GameScreen

class SnakeGame : Game() {

    override fun create() {
        setScreen(GameScreen())
    }

}
