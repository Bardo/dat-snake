package com.mygdx.game.core

/**
 * Created by stanislavvasylkovskyi on 11/19/16.
 */

enum class Directions {
    RIGHT,
    LEFT,
    UP,
    DOWN,
    NONE
}
