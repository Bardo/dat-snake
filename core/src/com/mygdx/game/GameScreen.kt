package com.mygdx.game


import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.MathUtils
import com.mygdx.game.controllers.Bot
import com.mygdx.game.controllers.CollisionController
import com.mygdx.game.controllers.DirectionController
import com.mygdx.game.core.Directions
import com.mygdx.game.core.GAME_STATE
import com.mygdx.game.objects.Panda
import com.mygdx.game.objects.Snake

import java.util.Random

/**
 * Created by stanislavvasylkovskyi on 11/18/16.
 */

class GameScreen : ScreenAdapter() {

    private val GRID_CELL = 15

    private var timer = MOVE_TIME

    private val batch = SpriteBatch()

    private val snake = Snake()
    private val panda = Panda()

    private val collisionController = CollisionController()
    private val directionController = DirectionController()

    private val bot = Bot()
    private val random = Random()
    private var color = Color(random.nextFloat(), random.nextFloat(), random.nextFloat(), random.nextFloat())

    private val r: Float = 0.toFloat()
    private val g: Float = 0.toFloat()
    private val b: Float = 0.toFloat()
    private val step = 0.03f

    internal var direction = Directions.NONE

    private var state = GAME_STATE.PLAYING

    private var bitmapFont: BitmapFont? = null

    private val layout = GlyphLayout()

    private val shapeRenderer = ShapeRenderer()

    private var score = 0

    override fun show() {
        bitmapFont = BitmapFont()
        panda.checkAndPlacePanda(snake)
        snake.addPart()
        snake.addPart()
    }

    override fun render(delta: Float) {
        var delta = delta
        val enableBot = false

        if (snake.hasHit())
            state = GAME_STATE.GAME_OVER

        when (state) {
            GAME_STATE.PLAYING -> {
                if (!enableBot) {
                    direction = directionController.snakeDirection
                }
                val deltaChange = directionController.deltaChange
                delta += deltaChange
                timer -= delta
                if (timer <= 0) {
                    timer = MOVE_TIME
                    if (enableBot) {
                        direction = bot.getDirection(snake, panda)
                    }
                    snake.setSnakeDirection(direction)
                    snake.move()
                    setUpColor()
                }
                if (collisionController.checkPandaCollision(snake, panda)) {
                    addToScore()
                }
            }
            GAME_STATE.GAME_OVER -> {
                checkForRestart()
            }
        }

        clearScreen()
        //        drawGrid();
        draw()
    }

    private fun draw() {
        batch.begin()
        snake.draw(batch)
        panda.draw(batch)
        when (state) {
            GAME_STATE.GAME_OVER -> {
                layout.setText(bitmapFont!!, GAME_OVER_TEXT)
                bitmapFont!!.draw(batch, GAME_OVER_TEXT, (Gdx.graphics.width - layout.width) / 2, (Gdx.graphics.height - layout.height) / 2)
            }
            GAME_STATE.PLAYING -> {}
        }
        drawScore()
        batch.end()
    }

    private fun setUpColor() {
        color = Color.BLACK
        //                new Color(mutateColor(color.r), mutateColor(color.g), mutateColor(color.b), 0.5f);
    }

    private fun clearScreen() {
        setUpColor()
        Gdx.gl.glClearColor(color.r, color.g,
                color.b, color.a)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
    }

    private fun drawGrid() {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line)
        var x = 0
        while (x < Gdx.graphics.width) {
            var y = 0
            while (y < Gdx.graphics.height) {
                shapeRenderer.rect(x.toFloat(), y.toFloat(), GRID_CELL.toFloat(), GRID_CELL.toFloat(), Color.BROWN, Color.GRAY, Color.OLIVE, Color.FIREBRICK)
                y += GRID_CELL
            }
            x += GRID_CELL
        }
        shapeRenderer.end()
    }

    private fun mutateColor(c: Float): Float {

        if (c + step > 1)
            return c - step * 5
        if (c - step < 0)
            return c + step * 5

        return if (MathUtils.random(1) == 1) c + step else c - step
    }

    private fun doRestart() {
        state = GAME_STATE.PLAYING
        snake.clearBody()
        snake.setSnakeDirection(Directions.RIGHT)
        snake.resetStartCoords()
        panda.isPandaAvailable = false
        panda.checkAndPlacePanda(snake)
        score = 0
    }

    private fun checkForRestart() {
        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) doRestart()
    }

    private fun addToScore() {
        score += POINTS_PER_PANDA
    }

    private fun drawScore() {
        if (state == GAME_STATE.PLAYING) {
            val scoreAsString = Integer.toString(score)
            layout.setText(bitmapFont!!, scoreAsString)
            bitmapFont!!.draw(batch, scoreAsString, (Gdx.graphics.width - layout.width) / 2, 4 * Gdx.graphics.height / 5 - layout.height / 2)
        }
    }

    companion object {

        private val GAME_OVER_TEXT = "Game Over... Tap space\n" + "   to restart!"

        private val MOVE_TIME = 1f

        private val POINTS_PER_PANDA = 20
    }


}
