package com.mygdx.game.objects

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.utils.Array
import com.mygdx.game.core.Directions

import com.mygdx.game.core.Directions.DOWN
import com.mygdx.game.core.Directions.LEFT
import com.mygdx.game.core.Directions.RIGHT
import com.mygdx.game.core.Directions.UP

/**
 * Created by stanislavvasylkovskyi on 11/19/16.
 */

class Snake {

    var snakeX = 0
        private set
    var snakeY = 0
        private set

    private var snakeDirection = RIGHT

    private val snakeHead = Texture(Gdx.files.internal("snakehead.png"))
    private val snakeBody = Texture(Gdx.files.internal("snakebody.png"))

    private val bodyParts = Array<BodyPart>()

    private var snakeXBeforeUpdate = 0
    private var snakeYBeforeUpdate = 0

    private var hasHit = false


    fun addPart() {
        val bodyPart = BodyPart(snakeBody)
        bodyPart.updateBodyPosition(snakeX, snakeY)
        bodyParts.insert(0, bodyPart)
    }

    fun move() {
        if (hasHit) return
        println("Moving...")
        snakeXBeforeUpdate = snakeX
        snakeYBeforeUpdate = snakeY
        when (this.snakeDirection) {
            RIGHT -> {
                snakeX += SNAKE_MOVEMENT
            }
            LEFT -> {
                snakeX -= SNAKE_MOVEMENT
            }
            UP -> {
                snakeY += SNAKE_MOVEMENT
            }
            DOWN -> {
                snakeY -= SNAKE_MOVEMENT
            }
            Directions.NONE -> {
            }
        }
        checkSnakeForOutOfBounds()
        updateBodyPartsPosition()
    }

    private fun updateBodyPartsPosition() {
        if (bodyParts.size > 0) {
            val bodyPart = bodyParts.removeIndex(0)
            bodyPart.updateBodyPosition(snakeXBeforeUpdate,
                    snakeYBeforeUpdate)
            bodyParts.add(bodyPart)
        }
    }

    private fun checkSnakeForOutOfBounds() {
        if (snakeX >= Gdx.graphics.width) {
            snakeX = 0
        }
        if (snakeX < 0) {
            snakeX = Gdx.graphics.width - SNAKE_MOVEMENT
        }
        if (snakeY >= Gdx.graphics.height) {
            snakeY = 0
        }
        if (snakeY < 0) {
            snakeY = Gdx.graphics.height - SNAKE_MOVEMENT
        }
    }

    fun draw(batch: Batch) {
        batch.draw(snakeHead, snakeX.toFloat(), snakeY.toFloat())
        for (bodyPart in bodyParts) {
            bodyPart.draw(batch)
        }
    }

    private fun getFutureCoords(direction: Directions): Coords {
        var x = snakeX
        var y = snakeY

        when (direction) {
            RIGHT -> {
                x += SNAKE_MOVEMENT
            }
            LEFT -> {
                x -= SNAKE_MOVEMENT
            }
            UP -> {
                y += SNAKE_MOVEMENT
            }
            DOWN -> {
                y -= SNAKE_MOVEMENT
            }
            Directions.NONE -> {}
        }

        return Coords(x, y)
    }


    private fun updateIfNotOppositeDirection(newSnakeDirection: Directions,
                                             oppositeDirection: Directions) {
        if ((snakeDirection != oppositeDirection || bodyParts.size == 0) && noColllisionWithBody(newSnakeDirection)) {
            println("setting direction " + newSnakeDirection.name)
            snakeDirection = newSnakeDirection
        }
    }

    private fun noColllisionWithBody(newSnakeDirection: Directions): Boolean {
        val nextStep = getFutureCoords(newSnakeDirection)

        for (i in 0..bodyParts.size - 1) {
            val bodyPart = bodyParts.get(i)
            if (bodyPart.x == nextStep.x && bodyPart.y == nextStep.y) {
                this.hasHit = true
                return false
            }
        }
        return true
    }


    fun setSnakeDirection(newSnakeDirection: Directions) {
        if (hasHit) return
        if (snakeDirection != newSnakeDirection) {
            when (newSnakeDirection) {
                LEFT -> {
                    updateIfNotOppositeDirection(newSnakeDirection, RIGHT)
                }
                RIGHT -> {
                    updateIfNotOppositeDirection(newSnakeDirection, LEFT)
                }
                UP -> {
                    updateIfNotOppositeDirection(newSnakeDirection, DOWN)
                }
                DOWN -> {
                    updateIfNotOppositeDirection(newSnakeDirection, UP)
                }
                Directions.NONE -> {
                }
            }
        }
    }

    fun hasHit(): Boolean {
        return hasHit
    }

    fun clearBody() {
        bodyParts.clear()
    }

    fun resetStartCoords() {
        snakeX = 0
        snakeY = 0
        snakeXBeforeUpdate = 0
        snakeYBeforeUpdate = 0
        hasHit = false
    }

    private inner class BodyPart(private val texture: Texture) {
        var x: Int = 0
            private set
        var y: Int = 0
            private set

        fun updateBodyPosition(x: Int, y: Int) {
            this.x = x
            this.y = y
        }

        fun draw(batch: Batch) {
            if (!(x == snakeX && y == snakeY))
                batch.draw(texture,
                        x.toFloat(), y.toFloat())
        }
    }

    private inner class Coords(val x: Int, val y: Int)

    companion object {

        val SNAKE_MOVEMENT = 15
    }
}
