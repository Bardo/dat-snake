package com.mygdx.game.objects

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.MathUtils
import com.mygdx.game.objects.Snake.Companion.SNAKE_MOVEMENT

/**
 * Created by stanislavvasylkovskyi on 11/19/16.
 */

class Panda {

    private val panda = Texture(Gdx.files.internal("panda.png"))

    var isPandaAvailable = false
    var pandaX: Int = 0
        private set
    var pandaY: Int = 0
        private set

    fun draw(batch: Batch) {
        if (isPandaAvailable) {
            batch.draw(panda, pandaX.toFloat(), pandaY.toFloat())
        }
    }

    fun checkAndPlacePanda(snake: Snake) {
        if (!isPandaAvailable) {
            do {
                pandaX = MathUtils.random(Gdx.graphics.width / SNAKE_MOVEMENT - 1) * SNAKE_MOVEMENT
                pandaY = MathUtils.random(Gdx.graphics.height / SNAKE_MOVEMENT - 1) * SNAKE_MOVEMENT
                isPandaAvailable = true
            } while (pandaX == snake.snakeX && pandaY == snake.snakeY)
        }
    }
}
